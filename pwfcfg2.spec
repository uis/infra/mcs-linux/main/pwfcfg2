Name: pwfcfg2
Version: 2.7
Release: 0
Source0: %{name}-%{version}.tgz
Copyright: GPL
Summary: Configuration files for various apps
Group: System Environment/Base
Distribution: PWF Linux
Vendor: University of Cambridge
Packager: Unix Support <unix-support@ucs.cam.ac.uk>
#Requires: xinitrc, fvwm2, fvwm2-icons, initscripts, sysklogd, setup, pwf-scripts >= 1.1, kdelibs, acroread, microsoft-ttf, pam, ntp, exim, mozilla = 1.2.1, fam, xinetd, pam-modules, OpenOffice_org
PreReq: %{insserv_prereq} mozilla
Requires: pwf-indexhtml
BuildRoot: %{_tmppath}/%{name}-root
BuildArchitectures: noarch

%Description
This package replaces a whole swathe of configuration files for 
use on the PWF.  This is designed to be modified for particular
domains.

%Prep
%setup

%Install
umask 022
rm -rf ${RPM_BUILD_ROOT}
mkdir -p ${RPM_BUILD_ROOT}/etc/pwf-linux
echo "REL=%{name}-%{version}-%{release}
PWFCFG_LOC=/etc/%{name}-%{version}-%release" > ${RPM_BUILD_ROOT}/etc/pwf-linux/%{name}
cd ${RPM_BUILD_ROOT}/etc && tar -xzf ${RPM_SOURCE_DIR}/%{name}-%{version}.tgz
mv %{name}-%{version} %{name}-%{version}-%{release}
#touch /etc/pwf-linux/randomfile-%{release}
#echo This is random data $RANDOM > ${RPM_BUILD_ROOT}/etc/pwf-linux/randomfile-%{release}

%Post
exec 1> /etc/pwf-linux/pwfcfg2.log 2>&1
echo Post install for %{name}-%{version}-%{release}
set -x
[ -f /var/lock/subsys/pwfcfg2 ] && rm -f /var/lock/subsys/pwfcfg2
echo Massaging US.jar file from mozilla
mkdir /etc/%{name}-%{version}-%{release}/opt/mozilla/lib/chrome
cd /etc/%{name}-%{version}-%{release}/opt/mozilla/lib/chrome
unzip /opt/mozilla/lib/chrome/US.jar
cp locale/US/navigator-region/region.properties .
sed -e 's,^startup.homepage_override_url.*,startup.homepage_override_url=file:///etc/pwf-linux/index.html,' \
    -e 's,^browser.startup.homepage.*,browser.startup.homepage=file:///etc/pwf-linux/index.html,' \
    < region.properties > locale/US/navigator-region/region.properties
zip US.jar -r locale
rm -rf locale
rm -rf region.properties
cd /
insserv acct
insserv xntpd
insserv -r xinetd
/bin/bash  -x /etc/%{name}-%{version}-%{release}/etc/init.d/pwfcfg2 start

%fillup_and_insserv -f pwfcfg2

#%Pre
#exec 1>> /etc/pwf-linux/pwfcfg2.log 2>&1
#echo this is the Pre-install... for %{name}-%{version}-%{release}

%PreUn
#exec 1>> /etc/pwf-linux/pwfcfg2.log 2>&1
#echo This is the pre-un for %{name}-%{version}-%{release}
#set -x
#cp /etc/pwf-linux/randomfile-%{release} /tmp
rm -rf /etc/%{name}-%{version}-%{release}/opt/mozilla/lib/chrome/

%PostUn
%insserv_cleanup

#echo this is the PostUn-install for %{name}-%{version}-%{release} ... >>  /etc/pwf-linux/pwfcfg2.log 

%Files
%defattr(-,root,root)
/etc/pwf-linux/%{name}
/etc/%{name}-%{version}-%{release}
#/etc/pwf-linux/randomfile-%{release}
