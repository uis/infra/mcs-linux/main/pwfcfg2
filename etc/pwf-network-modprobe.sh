#!/bin/bash
#
# Network modules to try to load at boot
#
# Intel PCI cards
modprobe e100
modprobe e1000
# 3Com PCI cards
modprobe 3c59x
modprobe typhoon
modprobe acenic
# Broadcom chipset based gigabit cards
modprobe tg3
# PCNet32, as seen in VMWare
modprobe pcnet32
# RTL8139, as seen on cheap motherboards
modprobe rtl8139cp
modprobe rtl8139too
# SysKonect SK89xx, as seen on killer-tomato.csx.private
modprobe sk98lin
