######################################################################
#               Runtime configuration file for Exim 4                #
######################################################################

# $dotat: etc/exim/conf4.bal3,v 1.65 2003/08/14 18:42:38 fanf2 Exp $


# This is the Exim 4 configuration file I use on my workstation in the
# Computing Service. It should be suitable for most systems in Cambridge
# which operate as satellites of the CS's central email servers. I have
# had positive feedback from a couple of users, and any other comments
# and suggestions are welcome!
#
# The aim is to mostly operate as a normal Unix MTA, except that the
# machine doesn't have a valid email domain of its own and has to use a
# "smarthost" SMTP relay. Local email simply uses the machine's hostname
# for its domain, but this doesn't work in the outside world so
# rewriting is performed on outgoing email make the addresses valid. In
# order to do this the configuration borrows an external domain in which
# the machine's users (particularly its admin) have addresses. There's
# also a "smartuser" feature, to make it possible to email other users
# of the external domain as if they were local users.
#
# The configuration attempts to be as generic as possible. All the
# Cambridge-specific stuff is in the macros below, and most
# operating-system-specific stuff is assumed to be configured at
# compile-time. Systems may vary in the location of the aliases file and
# whether they have an appropriate wheel group. We also assume a BSD-style
# /var/mail (drwxrwxr-x and group mail rather than drwxrwxrwt) and that
# users have individual groups (we permit -rwxrwxr-x .forward files).
# The configuration supports maildir which isn't part of the default
# Exim compile-time configuration.
#
# Tony Finch <fanf2@cam.ac.uk> http://dotat.at/


# The "smarthost" to use for outgoing SMTP.
#
RELAY = smtp.hermes.cam.ac.uk

# Users in this group are granted special privilege.
#
WHEEL = root

# The location of the system aliases file on your computer. This should
# contain aliases for all system users (especially root) and for special
# local parts like postmaster and mailer-daemon, so that you don't end
# up sending system email to the wrong person, e.g. root@DOMAIN.
#
ALIASES = /etc/aliases

# The admin's email address.
#
ADMIN = ${lookup{root}lsearch{ALIASES}}

# This is the external domain that we use when rewriting outgoing email,
# so that the local hostname isn't used as an (invalid) email domain on
# externally-visible email.
#
DOMAIN = cam.ac.uk

# This is a regular expression that matches the local-parts of email
# addresses in the external domain. It's used in two situations:
# firstly, when rewriting local addresses on outgoing email, and
# secondly, when doing a "smartuser" match on an invalid local address
# to see if it should be redirected to the external domain.
#
# For Cambridge, a rough match for CRSIDs will do.
#
NONLOCAL = \N^[a-z]+[0-9]+$\N

# This is how we rewrite local addresses on outgoing email. If the local
# user is not also a non-local user, then their address is rewritten to
# the admin's externally-valid address. Otherwise only the domain needs
# to be fixed. This macro must contain no white space.
#
REWRITE_TO	= ${if!match{$local_part}{NONLOCAL}{ADMIN}{$local_part@DOMAIN}}

# Return paths are somewhat more tricky to deal with, because the
# expansion must also check that the domain part is a local one.
#
REWRITE_FROM	= ${if eq{$sender_address_domain}{$primary_hostname} \
		       {${if !match{$sender_address_local_part}{NONLOCAL} \
		             {ADMIN} {$sender_address_local_part@DOMAIN} }} \
		       fail }

######################################################################
#                    MAIN CONFIGURATION SETTINGS                     #
######################################################################

# Which domains are local, i.e. the hostname and localhost
# (the latter being for fetchmail users).
#
domainlist local = localhost : @


# Which ACL to use for SMTP RCPT and DATA commands.
#
acl_smtp_rcpt = acl_check_rcpt
acl_smtp_data = acl_check_data

# Do lots of DNS lookups.
#
host_lookup = *
helo_lookup_domains = *
helo_try_verify_hosts = *

# Do ident checks.
#
rfc1413_hosts = *
rfc1413_query_timeout = 10s

# Be verbose.
#
log_selector = +all


# Discard frozen messages that are older than 1s.
#
timeout_frozen_after = 1s


# Remove spurious CRs on non-SMTP email.
#
drop_cr

# Use all 8 bits of TCP.
#
accept_8bitmime


# Privileged users.
#
admin_groups = WHEEL
trusted_groups = WHEEL

# Don't deliver email to root.
#
never_users = root

# Drop privileges.  This means exim will not re-exec itself and that only
# remote deliveries are possible which is exactly what we want.
deliver_drop_privilege = true

######################################################################
#                       ACL CONFIGURATION                            #
#        Specifies access control lists for incoming SMTP email      #
######################################################################

begin acl


# This is the access control list used for every RCPT command in an incoming
# SMTP message. It is a stripped down version of the one from the default
# configuration.
#
acl_check_rcpt:

  # Accept if the source is the local machine, either not over TCP/IP,
  # or with a source address that's one of this machine's interfaces.
  #
  accept  hosts         = : @[]

  # Deny if the local part contains @ or % or / or | or !. These are rarely
  # found in genuine local parts, but are often tried by people looking to
  # circumvent relaying restrictions.
  #
  deny    local_parts   = ^.*[@%!/|]

  # Accept email to the local postmaster.
  #
  accept  domains       = +local
          local_parts   = postmaster

  # Deny unless the sender address can be verified.
  #
  require verify        = sender

  # Accept if the address is for this machine and the
  # recipient can be verified.
  #
  accept  domains       = +local
          verify        = recipient

  # Otherwise deny.
  #
  deny    message       = Invalid address


# This is the access control list used for the DATA of an incoming
# SMTP message. It checks that bounce messages (which don't have an
# envelope sender) have a valid sender address in the message header.
#
acl_check_data:
  accept  senders = !:
  accept  message = A valid sender header is required for bounces
          verify  = header_sender



######################################################################
#                      ROUTERS CONFIGURATION                         #
#               Specifies how addresses are handled                  #
######################################################################
#     THE ORDER IN WHICH THE ROUTERS ARE DEFINED IS IMPORTANT!       #
# An address is passed to each router in turn until it is accepted.  #
######################################################################

begin routers


# This router handles aliasing using a traditional aliases file.
#
# If any of your aliases expand to pipes or files, they will run as
# ${exim_user} unless you change that here or in the appropriate
# transport. Note that the transports listed below are the same as are
# used for .forward files; you might want to set up different ones for
# pipe and file deliveries from aliases.
#
system_aliases:
  driver = redirect
  data = ${lookup{$local_part}lsearch{ALIASES}}


# Email for unrecognized users is sent to an @DOMAIN address if their
# username is NONLOCAL.
#
smartuser:
  driver = redirect
  local_parts = NONLOCAL
  data = $local_part@DOMAIN


# Route all addresses to the central email relay.
#
route_remote:
  driver = manualroute
  route_data = RELAY
  transport = remote_smtp
  no_more



######################################################################
#                      TRANSPORTS CONFIGURATION                      #
######################################################################
#                       ORDER DOES NOT MATTER                        #
#     Only one appropriate transport is called for each delivery.    #
######################################################################

# A transport is used only when referenced from a router that successfully
# handles an address.

begin transports


# We rewrite addresses on outgoing messages so that responses are
# directed to the central email machines rather than to here.
#
remote_smtp:
  driver = smtp
  return_path = REWRITE_FROM
  headers_rewrite = *@+local REWRITE_TO



######################################################################
#                      RETRY CONFIGURATION                           #
######################################################################

begin retry

# This single retry rule applies to all domains and all errors.
# It means that no messages will ever be left on the queue.

# Domain        Error           Retries
# ------        -----           -------

*               *               

# End of Exim configuration file
