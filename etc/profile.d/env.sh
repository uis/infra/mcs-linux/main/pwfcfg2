# Global.  Everyone wants these.
NNTPSERVER=nntp-serv.cam.ac.uk
PS1='\h:\W\$ '

export NNTPSERVER PS1

# For the students who need access to teaching materials
UX=/ux
CLTEACH=${UX}/clteach
PHYTEACH=${UX}/physics

export UX CLTEACH PHYTEACH

# Don't do this, it means defptr doesn't work, and it isn't needed - JPK
# Default printer
#[ -f /etc/sysconfig/printer ] && . /etc/sysconfig/printer && export PRINTER

# Make gconf use local locking...
GCONF_LOCAL_LOCKS=1
export GCONF_LOCAL_LOCKS

