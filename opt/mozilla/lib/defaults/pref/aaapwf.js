// Preferences for Mozilla under PWF Linux

// Disable Mozilla's disk cache to avoid eating user's quota
pref("browser.cache.disk.enable", false);

// Use the University Web Cache
pref("network.proxy.autoconfig_url", "http://www.cam.ac.uk/proxyconfig.pac");
pref("network.proxy.type", 2);

// Mail configuration
// Doing this properly is hard at least.  For now, specify those things that
// seem to work correctly.

// Outgoing SMTP is via smtp.hermes.cam.ac.uk.
pref("mail.smtp.defaultserver", "hermes");
pref("mail.smtpserver.hermes.hostname", "smtp.hermes.cam.ac.uk");
pref("mail.smtpserver.hermes.try_ssl", 1);
